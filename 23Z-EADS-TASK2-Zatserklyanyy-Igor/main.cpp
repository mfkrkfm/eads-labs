#include <iostream>
#include <cassert>
#include <string>
#include "DoubleLinkedRing.h"

void testPushFront()
{
    DoubleLinkedRing<int, char> a;
    a.push_front(1, 'a');
    a.push_front(2, 'b');
    a.push_front(3, 'c');
    assert(a.get_size() == 3);
    DoubleLinkedRing<int, char>::iterator it = a.begin();
    for(int i = a.get_size(); i >= 1; i--, it++)
    {
        assert(*it == i);
    }
}

void testPushBack()
{
    DoubleLinkedRing<int, char> a;
    a.push_back(1, 'a');
    a.push_back(2, 'b');
    a.push_back(3, 'c');
    assert(a.get_size() == 3);
    DoubleLinkedRing<int, char>::iterator it = a.begin();
    for(int i = 1; i <= a.get_size(); i++, it++)
    {
        assert(*it == i);
    }
}

void testPopFront()
{
    DoubleLinkedRing<int, char> a;
    a.push_back(1, 'a');
    a.push_back(2, 'b');
    a.push_back(3, 'c');
    a.pop_front();
    assert(a.get_size() == 2);
    DoubleLinkedRing<int, char>::iterator it = a.pop_front();
    assert(a.get_size() == 1);
    assert(*it == 3);
}

void testPopBack()
{
    DoubleLinkedRing<int, char> a;
    a.push_back(1, 'a');
    a.push_back(2, 'b');
    a.push_back(3, 'c');
    a.pop_back();
    assert(a.get_size() == 2);
    DoubleLinkedRing<int, char>::iterator it = a.pop_back();
    assert(a.get_size() == 1);
    assert(*it == 1);
}

void testInsert()
{
    DoubleLinkedRing<int, char> a;
    a.push_back(1, 'a');
    DoubleLinkedRing<int, char>::iterator it = a.push_back(2, 'b');
    a.push_back(3, 'c');
    it = a.insert(it, 4, 'd');
    assert(a.get_size() == 4);
    assert(*it == 4);
}

void testErase()
{
    DoubleLinkedRing<int, char> a;
    a.push_back(1, 'a');
    DoubleLinkedRing<int, char>::iterator it = a.push_back(2, 'b');
    a.push_back(3, 'c');
    it = a.erase(it);
    assert(a.get_size());
    assert(*it == 3);
}

void testCopyConstructor()
{
    DoubleLinkedRing<int, char> a;
    a.push_back(1, 'a');
    a.push_back(2, 'b');
    a.push_back(3, 'c');
    assert(a.get_size() == 3);
    DoubleLinkedRing<int, char> b(a);
    assert(b.get_size() == 3);
    DoubleLinkedRing<int, char>::iterator it = b.begin();
    for(int i = 1; i <= b.get_size(); i++, it++)
    {
        assert(*it == i);
    }
}
void testAssignOperator()
{
    DoubleLinkedRing<int, char> a;
    a.push_back(1, 'a');
    a.push_back(2, 'b');
    a.push_back(3, 'c');
    assert(a.get_size() == 3);
    DoubleLinkedRing<int, char> b = a;
    assert(b.get_size() == 3);
    DoubleLinkedRing<int, char>::iterator it = b.begin();
    for(int i = 1; i <= b.get_size(); i++, it++)
    {
        assert(*it == i);
    }
    DoubleLinkedRing<int, char> c;
    c.push_back(4, 'd');
    a = c;
    assert(a.get_size() == 1);
}

void testAdditionOperator()
{
    DoubleLinkedRing<int, char> a;
    a.push_back(1, 'a');
    a.push_back(2, 'b');
    a.push_back(3, 'c');
    DoubleLinkedRing<int, char> b;
    b.push_back(4, 'a');
    b.push_back(5, 'b');
    b.push_back(6, 'c');
    DoubleLinkedRing<int, char> c = a + b;
    DoubleLinkedRing<int, char>::iterator it = c.begin();
    for(int i = 1; i <= c.get_size(); i++, it++)
    {
        assert(*it == i);
    }
    DoubleLinkedRing<int, char> d;
    assert((a + d).get_size() == 3);
    assert((d + a).get_size() == 3);
    c += a;
    assert(c.get_size() == 9);
    assert(c.end().getKey() == 3);
}

void testEqualityOperators()
{
    DoubleLinkedRing<int, char> a;
    DoubleLinkedRing<int, char> b;
    assert(a == b);
    DoubleLinkedRing<int, char>::iterator it1 = a.push_back(1, 'a');
    assert(a != b);
    a.push_back(2, 'b');
    a.push_back(3, 'c');
    DoubleLinkedRing<int, char>::iterator it2 = b.push_back(1, 'a');
    DoubleLinkedRing<int, char>::iterator it3 = b.push_back(2, 'b');
    b.push_back(3, 'c');
    assert(it1 == it2);
    assert(it1 != it3);
    assert(a == b);
    assert(!(a != b));
    b.pop_front();
    assert(a != b);
    assert(!(a == b));
}

void testEmpty()
{
    DoubleLinkedRing<int, char> a;
    a.push_back(1, 'a');
    a.push_back(2, 'b');
    a.push_back(3, 'c');
    assert(a.empty() == false);
}

void testClear()
{
    DoubleLinkedRing<int, char> a;
    a.push_back(1, 'a');
    a.push_back(2, 'b');
    a.push_back(3, 'c');
    assert(a.get_size() == 3);
    a.clear();
    assert(a.get_size() == 0);
}

void testFilter()
{
    DoubleLinkedRing<std::string, int> source;
    source.push_back("uno", 1);
    source.push_back("due", 2);
    source.push_back("tre", 3);
    source.push_back("quattro", 4);
    source.push_back("cinque", 5);
    source.push_back("sei", 6);
    source.push_back("sette", 7);
    source.push_back("otto", 8);
    DoubleLinkedRing<std::string, int> filteredResult = filter<std::string, int>(source, [](const std::string& str) { return str.size() > 3; });
    DoubleLinkedRing<std::string, int> correctResult;
    correctResult.push_back("quattro", 4);
    correctResult.push_back("cinque", 5);
    correctResult.push_back("sette", 7);
    correctResult.push_back("otto", 8);
    assert(filteredResult == correctResult);
}

void testJoin()
{
    DoubleLinkedRing<std::string, int> first;
    first.push_back("uno", 1);
    first.push_back("due", 1);
    first.push_back("tre", 2);
    first.push_back("quattro", 1);
    DoubleLinkedRing<std::string, int> second;
    second.push_back("due", 1);
    second.push_back("tre", 1);
    second.push_back("quattro", 3);
    second.push_back("cinque", 5);
    auto joinedRing = join(first, second);
    DoubleLinkedRing<std::string, int> correctResult;
    correctResult.push_back("uno", 1);
    correctResult.push_back("due", 2);
    correctResult.push_back("tre", 3);
    correctResult.push_back("quattro", 4);
    correctResult.push_back("cinque", 5);
    assert(joinedRing == correctResult);
}

void testUnique()
{
    DoubleLinkedRing<std::string, std::string> src;
    src.push_back("one", "uno");
    src.push_back("two", "due");
    src.push_back("three", "tre");
    src.push_back("one", "eins");
    src.push_back("two", "zwei");
    src.push_back("three", "drei");
    src.push_back("four", "vier");
    src.push_back("five", "cinque");
    src.push_back("six", "sechs");
    src.push_back("seven", "sieben");
    src.push_back("acht", "otto");
    DoubleLinkedRing<std::string, std::string> result = unique<std::string, std::string>(src,
        [](const std::string&, const std::string& i1, const std::string& i2)
        {
            return i1 + "-" + i2;
        }
    );
    DoubleLinkedRing<std::string, std::string> correctResult;
    correctResult.push_back("one", "uno-eins");
    correctResult.push_back("two", "due-zwei");
    correctResult.push_back("three", "tre-drei");
    correctResult.push_back("four", "vier");
    correctResult.push_back("five", "cinque");
    correctResult.push_back("six", "sechs");
    correctResult.push_back("seven", "sieben");
    correctResult.push_back("acht", "otto");
    assert(result == correctResult);

}

void testShuffle()
{
    DoubleLinkedRing<std::string, int> first;
    DoubleLinkedRing<std::string, int> second;
    first.push_back("uno", 1);
    first.push_back("due", 2);
    first.push_back("tre", 3);
    first.push_back("quattro", 4);
    second.push_back("bir", 1);
    second.push_back("iki", 2);
    second.push_back("uc", 3);
    second.push_back("dort", 4);
    second.push_back("bes", 5);
    auto result = shuffle(first, 1, second, 2, 3);
    DoubleLinkedRing<std::string, int> correctResult;
    correctResult.push_back("uno", 1);
    correctResult.push_back("bir", 1);
    correctResult.push_back("iki", 2);
    correctResult.push_back("due", 2);
    correctResult.push_back("uc", 3);
    correctResult.push_back("dort", 4);
    correctResult.push_back("tre", 3);
    correctResult.push_back("bes", 5);
    correctResult.push_back("bir", 1);
    assert(result == correctResult);
}

void additionalFunctions()
{
    DoubleLinkedRing<std::string, int> source;
    source.push_back("uno", 1);
    source.push_back("due", 2);
    source.push_back("tre", 3);
    source.push_back("quattro", 4);
    source.push_back("cinque", 5);
    source.push_back("sei", 6);
    source.push_back("sette", 7);
    source.push_back("otto", 8);
    filter<std::string, int>(source, [](const std::string& str) { return str.size() > 3; }).display();

    std::cout << "---------------------------------------------------------------------" << std::endl;

    DoubleLinkedRing<std::string, std::string> src;
    src.push_back("one", "uno");
    src.push_back("two", "due");
    src.push_back("three", "tre");
    src.push_back("one", "eins");
    src.push_back("two", "zwei");
    src.push_back("three", "drei");
    src.push_back("four", "vier");
    src.push_back("five", "cinque");
    src.push_back("six", "sechs");
    src.push_back("seven", "sieben");
    src.push_back("acht", "otto");
    unique<std::string, std::string>(src, [](const std::string&, const std::string& i1, const std::string& i2) { return i1 + "-" + i2; }).display();

    std::cout << "---------------------------------------------------------------------" << std::endl;

    DoubleLinkedRing<std::string, int> first;
    DoubleLinkedRing<std::string, int> second;
    first.push_back("uno", 1);
    first.push_back("due", 2);
    first.push_back("tre", 3);
    first.push_back("quattro", 4);
    second.push_back("bir", 1);
    second.push_back("iki", 2);
    second.push_back("uc_", 3); // '_' only for output
    second.push_back("dort", 4);
    second.push_back("bes", 5);
    shuffle(first, 1, second, 2, 3).display();

}

void runAllTests()
{
    testPushFront();
    testPushBack();
    testPopFront();
    testPopBack();
    testCopyConstructor();
    testAssignOperator();
    testAdditionOperator();
    testEqualityOperators();
    testEmpty();
    testClear();
    testInsert();
    testErase();
    testFilter();
    testJoin();
    testUnique();
}

int main(void)
{
    runAllTests();
    std::cout << "TESTS PASSED\n";
    //additionalFunctions();
    DoubleLinkedRing<float, float> first;
    first.push_back(1.0, 4.0);
    first.push_back(7.2, 2.1);
    first.push_back(10.5, 4.7);
    DoubleLinkedRing<float, float> second;
    second.push_back(2.0, 1.0);
    second.push_back(8.0, 4.5);
    auto result = iunion(first, second);
    result.display();


    return 0;
}