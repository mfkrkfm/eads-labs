#include <iostream>
#include <string>

template <typename Key, typename Info>
class DoubleLinkedRing
{
private:
	struct Node
	{
	    Key key;
	    Info info;
		Node* next;
	    Node* prev;
	    Node(Key key, Info  info);
	    void display();
	};
	Node* head;
	unsigned int size;
public:
	class iterator
	{
	private:
        DoubleLinkedRing::Node* current;
	public:
	    iterator(DoubleLinkedRing::Node* node);
        iterator();
        iterator(const iterator& other);
        iterator& operator++();
        iterator operator++(int);
		iterator& operator--();
        iterator operator--(int);
        bool operator==(const iterator& other) const;
        bool operator!=(const iterator& other) const;
		Info operator+(const Info& other) const;
		Key operator*();
		Key getKey() const;
		Info getInfo() const;
		bool isNull() const;
        friend class DoubleLinkedRing;
	};
	class const_iterator
	{

	};
	DoubleLinkedRing();
	DoubleLinkedRing(const DoubleLinkedRing& src);
	~DoubleLinkedRing();
	DoubleLinkedRing& operator=(const DoubleLinkedRing& src);
	DoubleLinkedRing operator+(const DoubleLinkedRing& src);
	DoubleLinkedRing& operator+=(const DoubleLinkedRing& src);
	bool operator==(const DoubleLinkedRing& src);
	bool operator!=(const DoubleLinkedRing& src);
	iterator push_front(const Key& key, const Info& info);
	iterator push_back(const Key& key, const Info& info);
	iterator pop_front();
	iterator pop_back();
	iterator insert(iterator position, const Key& key, const Info& info);
	iterator erase(iterator position);
	iterator begin() const;
	iterator end() const;
	iterator find_key(const Key& key) const;
	bool empty() const;
	bool clear();
	unsigned int get_size();
	void display();
	void display_reversed();
};

//Node
template <typename Key, typename Info>
DoubleLinkedRing<Key, Info>::Node::Node(Key key, Info  info)
{
    this->key = key;
    this->info = info;
    next = nullptr;
    prev = nullptr;
}

template <typename Key, typename Info>
void DoubleLinkedRing<Key, Info>::Node::display()
{
    std::cout << "Key: " << key << "\tInfo: " << info << "\n";
}

//Iterator
template <typename Key, typename Info>
DoubleLinkedRing<Key, Info>::iterator::iterator(DoubleLinkedRing<Key, Info>::Node* node)
{
	current = node;
}

template <typename Key, typename Info>
DoubleLinkedRing<Key, Info>::iterator::iterator()
{
	current = nullptr;
}

template <typename Key, typename Info>
DoubleLinkedRing<Key, Info>::iterator::iterator(const iterator& other)
{
	current = other.current;
}

template <typename Key, typename Info>
DoubleLinkedRing<Key, Info>::iterator& DoubleLinkedRing<Key, Info>::iterator::operator++()
{
    current = current->next;
    return *this;
}

template <typename Key, typename Info>
DoubleLinkedRing<Key, Info>::iterator DoubleLinkedRing<Key, Info>::iterator::operator++(int) 
{
    iterator temp(*this);
    ++(*this);
    return temp;
}

template <typename Key, typename Info>
DoubleLinkedRing<Key, Info>::iterator& DoubleLinkedRing<Key, Info>::iterator::operator--()
{
    current = current->prev;
    return *this;
}

template <typename Key, typename Info>
DoubleLinkedRing<Key, Info>::iterator DoubleLinkedRing<Key, Info>::iterator::operator--(int) 
{
    iterator temp(*this);
    --(*this);
    return temp;
}

template <typename Key, typename Info>
bool DoubleLinkedRing<Key, Info>::iterator::operator==(const iterator& other) const 
{
	if(current->key == other.current->key && current->info == other.current->info)
		return true;
	else
		return false;
}

template <typename Key, typename Info>
bool DoubleLinkedRing<Key, Info>::iterator::operator!=(const iterator& other) const 
{
    if(current->key == other.current->key && current->info == other.current->info)
		return false;
	else
		return true;
}
template <typename Key, typename Info>
Info DoubleLinkedRing<Key, Info>::iterator::operator+(const Info& other) const
{
	return current->info + other;
}

template <typename Key, typename Info>
Key DoubleLinkedRing<Key, Info>::iterator::operator*()
{
	return current->key;
}

template <typename Key, typename Info>
Key DoubleLinkedRing<Key, Info>::iterator::getKey() const
{
	return current->key;
}
template <typename Key, typename Info>
Info DoubleLinkedRing<Key, Info>::iterator::getInfo() const
{
	return current->info;
}

template <typename Key, typename Info>
bool DoubleLinkedRing<Key, Info>::iterator::isNull() const
{
	if(current == nullptr)
		return true;
	else
		return false;
}

//DoubleLinkedRing

template <typename Key, typename Info>
DoubleLinkedRing<Key, Info>::DoubleLinkedRing()
{
	head = nullptr;
	size = 0;
}
template <typename Key, typename Info>
DoubleLinkedRing<Key, Info>::DoubleLinkedRing(const DoubleLinkedRing& src)
{
	head = nullptr;
	size = 0;
	if (src.size > 0)
    {
        Node* srcCurrent = src.head->next;
		push_back(src.head->key, src.head->info);
        while (srcCurrent != src.head)
        {
            push_back(srcCurrent->key, srcCurrent->info);
			srcCurrent = srcCurrent->next;
        }
    }
}
template <typename Key, typename Info>
DoubleLinkedRing<Key, Info>::~DoubleLinkedRing()
{
	clear();
}

template <typename Key, typename Info>
DoubleLinkedRing<Key, Info>& DoubleLinkedRing<Key, Info>::operator=(const DoubleLinkedRing& src)
{
    if(this == &src)
        return *this;
    clear();
	head = nullptr;
    if (src.size > 0)
    {
        Node* srcCurrent = src.head->next;
		push_back(src.head->key, src.head->info);
        while (srcCurrent != src.head)
        {
            push_back(srcCurrent->key, srcCurrent->info);
			srcCurrent = srcCurrent->next;
        }
    }
    return *this;
}

template <typename Key, typename Info>
DoubleLinkedRing<Key, Info> DoubleLinkedRing<Key, Info>::operator+(const DoubleLinkedRing<Key, Info>& src)
{
	if(src.empty())
		return *this;
	if(this->empty())
		return src;
	DoubleLinkedRing<Key, Info> newRing;
	iterator it = begin();
	do
	{
		newRing.push_back(it.getKey(), it.getInfo());
		it++;
	}
	while(it != begin());
	iterator itsrc = src.begin();
	do
	{
		newRing.push_back(itsrc.getKey(), itsrc.getInfo());
		itsrc++;
	}
	while(itsrc != src.begin());
	return newRing;
}

template <typename Key, typename Info>
DoubleLinkedRing<Key, Info>& DoubleLinkedRing<Key, Info>::operator+=(const DoubleLinkedRing<Key, Info>& src)
{
	*this = *this + src;
	return *this;
}

template <typename Key, typename Info>
bool DoubleLinkedRing<Key, Info>::operator==(const DoubleLinkedRing& src)
{
	if(this->size != src.size)
		return false;
	if(this->size == 0 && src.size == 0)
		return true;
	iterator it1 = begin();
	iterator it2 = begin();
	do
	{
		if(it1 != it2)
			return false;	
		it1++;
		it2++; 
	}
	while(it1 != begin());
	return true;
}
template <typename Key, typename Info>
bool DoubleLinkedRing<Key, Info>::operator!=(const DoubleLinkedRing& src)
{
	if(*this == src)
		return false;
	else
		return true;
}

template <typename Key, typename Info>
DoubleLinkedRing<Key, Info>::iterator DoubleLinkedRing<Key, Info>::push_front(const Key& key, const Info& info)
{
	DoubleLinkedRing<Key, Info>::Node* newNode = new Node(key, info);
	switch(size)
	{
		case 0:
			head = newNode;
			head->next = head->prev = head;
			break;
		case 1:
			head->next = newNode;
			head->prev = newNode;
			newNode->next = newNode->prev = head;
			head = newNode;
			break;
		default:
			newNode->prev = head->prev;
			head->prev->next = newNode;
			newNode->next = head;
			head->prev = newNode;
			head = newNode;
			break;
	}
	size++;
	return iterator(head);
}

template <typename Key, typename Info>
DoubleLinkedRing<Key, Info>::iterator DoubleLinkedRing<Key, Info>::push_back(const Key& key, const Info& info)
{
	DoubleLinkedRing<Key, Info>::Node* newNode= new Node(key, info);
	switch(size)
	{
		case 0:
			head = newNode;
			head->next = head->prev = head;
			break;
		case 1:
			head->next = newNode;
			head->prev = newNode;
			newNode->next = newNode->prev = head;
			break;
		default:
			newNode->prev = head->prev;
			head->prev->next = newNode;
			newNode->next = head;
			head->prev = newNode;
			break;
	}
	size++;
	return iterator(head->prev);
}

template <typename Key, typename Info>
DoubleLinkedRing<Key, Info>::iterator DoubleLinkedRing<Key, Info>::pop_front()
{
	switch(size)
	{
		case 0:
			break;
		case 1:
			delete head;
			head = nullptr;
			size--;
			break;
		default:
			DoubleLinkedRing<Key, Info>::Node* newHead = head->next;
			newHead->prev = head->prev;
			head->prev->next = newHead;
        	delete head;
        	head = newHead;
			size--;
			break;
	}
	return begin();
}

template <typename Key, typename Info>
DoubleLinkedRing<Key, Info>::iterator DoubleLinkedRing<Key, Info>::pop_back()
{
	switch(size)
	{
		case 0:
			break;
		case 1:
			delete head;
			head = nullptr;
			size--;
			break;
		default:
			DoubleLinkedRing<Key, Info>::Node* last = head->prev;
			last->prev->next = head;
			head->prev = last->prev;
        	delete last;
			size--;
			break;
	}
	return begin();
}

template <typename Key, typename Info>
DoubleLinkedRing<Key, Info>::iterator DoubleLinkedRing<Key, Info>::insert(iterator position, const Key& key, const Info& info)
{
	if(position.current == nullptr)
		return position;
    if(position.current == head->prev)
        return push_back(key, info);
    else
    {
        Node* newNode = new Node(key, info);
        Node* current = position.current;
        newNode->prev = current;
        newNode->next = current->next;
        current->next->prev = newNode;
        current->next = newNode;
        size++;
        return iterator(newNode);
    }
}

template <typename Key, typename Info>
DoubleLinkedRing<Key, Info>::iterator DoubleLinkedRing<Key, Info>::erase(iterator position)
{
	if(size == 0 || position.current == nullptr)
		return position;
    if(position.current == head)
		return pop_front();
    else
    {
        Node* current = position.current;
        current->prev->next = current->next;
        current->next->prev = current->prev;
        Node* nextNode = current->next;
        delete current;
        size--;
        return iterator(nextNode);
    }
}

template <typename Key, typename Info>
DoubleLinkedRing<Key, Info>::iterator DoubleLinkedRing<Key, Info>::begin() const
{
	return iterator(head);
}

template <typename Key, typename Info>
DoubleLinkedRing<Key, Info>::iterator DoubleLinkedRing<Key, Info>::end() const
{
	return iterator(head->prev);
}

template <typename Key, typename Info>
DoubleLinkedRing<Key, Info>::iterator DoubleLinkedRing<Key, Info>::find_key(const Key& key) const
{	
	iterator it;
	if(empty())
		return it;
	it = begin();
	do
	{
		if (it.getKey() == key)
			return it;
		it++;
	}
	while(it != begin());
	iterator it2;
	return it2;
}

template <typename Key, typename Info>
bool DoubleLinkedRing<Key, Info>::empty() const
{
	if(size == 0)
		return true;
	else
		return false;
}

template <typename Key, typename Info>
bool DoubleLinkedRing<Key, Info>::clear()
{
    if(!head)
        return false;
    while (head->next != head)
    {
        DoubleLinkedRing<Key, Info>::Node *temp = head;
        head = head->next;
        head->prev = temp->prev;
        temp->prev->next = head;
        delete temp;
    }
    delete head;
	head = nullptr;
    size = 0;
    return true;
}

template <typename Key, typename Info>
unsigned int DoubleLinkedRing<Key, Info>::get_size()
{
	return size;
}

template <typename Key, typename Info>
void DoubleLinkedRing<Key, Info>::display()
{
	if(size == 0)
	{
		std::cout << "Empty DoubleLinkedRing\n";
		return;
	}
	DoubleLinkedRing<Key, Info>::Node* current = head;
	do
	{
		current->display();
		current = current->next;
	}
	while(current != head);
}

template <typename Key, typename Info>
void DoubleLinkedRing<Key, Info>::display_reversed()
{
	if(size == 0)
	{
		std::cout << "Empty DoubleLinkedRing\n";
		return;
	}
	DoubleLinkedRing<Key, Info>::Node* current = head;
	do
	{
		current = current->prev;
		current->display();
	}
	while(current != head);
}

//ADDITIONAL FUNCTIONS

template<typename Key, typename Info>
DoubleLinkedRing<Key, Info> filter(const DoubleLinkedRing<Key, Info>& source, bool (pred)(const Key&))
{
    DoubleLinkedRing<Key, Info> result;
	if(source.empty())
		return result;
	auto it = source.begin();
    do
    {
        if(pred(it.getKey()))
        {
            result.push_back(it.getKey(), it.getInfo());
        }
		++it;
    }
	while(it != source.begin());

    return result;
}


template<typename Key, typename Info>
DoubleLinkedRing<Key, Info> join(const DoubleLinkedRing<Key, Info>& first, const DoubleLinkedRing<Key, Info>& second);

template<typename Key, typename Info>
DoubleLinkedRing<Key, Info> join(const DoubleLinkedRing<Key, Info>& first, const DoubleLinkedRing<Key, Info>& second)
{
	DoubleLinkedRing<Key, Info> result = second;
	DoubleLinkedRing<Key, Info> result2;
	if(first.empty() && !second.empty())
		return second;
	if(!(first.empty()) && second.empty())
		return first;
	if(first.empty() && second.empty())
		return result;
	auto it = first.begin();
    do
    {
        Key key = it.getKey();
        Info info = it.getInfo();
        auto secondIt = result.find_key(key);
		while(!secondIt.isNull())
		{
            info += secondIt.getInfo();
			result.erase(secondIt);
			secondIt = result.find_key(key);
		}
        result2.push_back(key, info);
		it++;
    }
	while(it != first.begin());
    return result2 + result;
}

template<typename Key, typename Info>
DoubleLinkedRing<Key, Info> unique(const DoubleLinkedRing<Key, Info>& source, Info (aggregate)(const Key&, const Info&, const Info&))
{
    DoubleLinkedRing<Key, Info> result;
    if (source.empty())
	{
        return result;
    }
    auto it = source.begin();
    do
	{
        Key currentKey = it.getKey();
        Info currentValue = it.getInfo();
        auto resultIt = result.find_key(currentKey);
        if (resultIt.isNull())
		{
            result.push_back(currentKey, currentValue);
        } 
		else
		{
            Info aggregatedValue = aggregate(currentKey, resultIt.getInfo(), currentValue);
            result.erase(resultIt);
            result.push_back(currentKey, aggregatedValue);
        }
        ++it;
    }
	while (it != source.begin());
    return result;
}


template<typename Key, typename Info>
DoubleLinkedRing<Key, Info> shuffle(const DoubleLinkedRing<Key, Info>& first, unsigned int fcnt, const DoubleLinkedRing<Key, Info>& second, unsigned int scnt, unsigned int reps)
{
	DoubleLinkedRing<Key, Info> result;
	auto it1 = first.begin();
	auto it2 = second.begin();
	for(int i = 0; i < reps; i++)
	{
		for(int j = 0; j < fcnt; j++)
		{
			result.push_back(it1.getKey(), it1.getInfo());
			it1++;
		}
		for(int j = 0; j < scnt; j++)
		{
			result.push_back(it2.getKey(), it2.getInfo());
			it2++;
		}
	}
	return result;
}

template<typename num>
DoubleLinkedRing<num, num> iunion(const DoubleLinkedRing<num, num>& first, const DoubleLinkedRing<num, num>& second)
{
	DoubleLinkedRing<num, num> result;
	num b;
	num l;
	auto it1 = first.begin();
	auto it2 = second.begin();
	do
	{
		if(it1.getKey() < it2.getKey())
		{
			b = it1.getKey();
			if((it1.getInfo()) < it2.getInfo())
				l = (it2.getKey() - it1.getKey()) + it2.getInfo();
			else
				l = it1.getInfo();
		}
		else
		{
			b = it2.getKey();
			if((it2.getInfo()) < it1.getInfo())
				l = (it1.getKey() - it2.getKey()) + it1.getInfo();
			else
				l = it2.getInfo();
		}
		it1++, it2++;
		if((b + l) > it1.getKey() || (b + l) > it2.getKey())
		{
			if(it1.getKey() + it1.getInfo() > it2.getKey() + it2.getInfo())
				l += (it1.getKey() + it1.getInfo() - (b+l));
			else
				l += (it2.getKey() + it2.getInfo() - (b+l));
		}
		result.push_back(b, l);
	}
	while(it1 != first.begin() && it2 != second.begin());
	return result;
}
