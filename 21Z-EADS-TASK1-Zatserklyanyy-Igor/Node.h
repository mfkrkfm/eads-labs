#include <iostream>

template <typename Key, typename Info>
struct Node
{
    Key key;
    Info info;
    Node* next;
    Node(Key key, Info  info);
    void display();
};

template <typename Key, typename Info>
Node<Key, Info>::Node(Key key, Info  info)
{
    this->key = key;
    this->info = info;
    next = nullptr;
}

template <typename Key, typename Info>
void Node<Key, Info>::display()
{
    std::cout << "Key: " << key << "\tInfo: " << info << std::endl;
}