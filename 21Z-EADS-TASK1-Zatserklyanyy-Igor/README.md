# Algorithm and Data Structure Task #1

## Part 1 - Design a class

Design a class to represent collection (singly linked list). Write unit tests for the designed class, at least one test per method.

```c++
template <typename Key, typename Info>
class Sequence
{
    // implemented as singly linked list
public:
    Sequence();

    Sequence(const Sequence& src)
    {
        // initialize properly data members
        // probably as in default constructor
        *this = src;
    }

    ~Sequence();

    Sequence& operator=(const Sequence& src);

    unsigned int size() const;
    void push_front(const Key& key, const Info& info);
    // keys are not unique!
    bool pop_front();

    // and the rest of the interface
private:

};
```

## Part 2 – Implement two additional functions
Write two additional (external) functions (not methods) and unit tests for them.

```c++
//Function #1 - create two sequences by splitting the existing one
//(move elements from the source collection seq
// to one of the target collectios: seq1, seq2
template <typename Key, typename Info>
void split_pos(const Sequence <Key, Info>& seq, int start_pos,
                int len1, int len2, int count,
                Sequence <Key, Info>& seq1, Sequence <Key, Info>& seq2)
{
    //…
}

// Function #2 - create two sequences by splitting the existing one
//(move elements from the source collection seq
// to one of the target collectios: seq1, seq2
template <typename Key, typename Info>
void split_key(const Sequence <Key, Info>& seq, const Key& start_key,
                int start_occ, int len1, int len2, int count,
                Sequence <Key, Info>& seq1, Sequence <Key, Info>& seq2)
{
    //…
}
```

### Example 1 for split_pos:
#### Parameters:
    seq={0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24},
    start_pos=2, len1=2, len2=3, count=4

#### Result:

    seq={0,1,22,23,24}
    seq1={2,3,7,8,12,13,17,18}
    seq2={4,5,6,9,10,11,14,15,16,19,20,21}

### Example 2 for split_pos:

#### Parameters:

    seq={0,1,2,3,4,5,6,7,8,9,10},
    start_pos=2, len1=2, len2=3, count=4

#### Result:

    seq={0,1}
    seq1={2,3,7,8}
    seq2={4,5,6,9,10}

### Example for split_key:

#### Parameters:

    seq={0,1,2,3,4,5,6,4,8,9,4,11,12,2,14,15,11,17,23,19,20,21,22,23,24},
    start_key=4, start_occ=2, len1=3, len2=2, count=2

#### Result:

    seq={0,1,2,3,4,5,6,17,23,19,20,21,22,23,24}
    seq1={4,8,9,12,2,14}
    seq2={4,11,15,11}
