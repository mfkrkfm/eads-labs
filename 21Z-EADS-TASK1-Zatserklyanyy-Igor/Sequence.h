#include "Node.h"
#include <iostream>

template <typename Key, typename Info>
class Sequence
{
private:
    Node<Key, Info>* head;
    unsigned int sequenceSize;
public:
    Sequence();
    Sequence(const Sequence& src);
    ~Sequence();
    Sequence& operator=(const Sequence& src);
    Sequence& operator+(const Sequence& src); //allows to concatenate sequences
    Sequence<Key, Info>& operator+=(const Sequence& src);
    unsigned int getSequenceSize() const; //returns size of the sequence
    Node<Key, Info>* getHead();
    Node<Key, Info>* getTail(); //returns pointer on last element
    void display();
    bool isEmpty(); //checks if sequence is empty
    Node<Key, Info>* find(const Key& key, const Info& info); //return pointer to first element with given Key and Info
    //unsigned int size() const;  // getSequenceSize() is used 
    void push_front(Key key, Info info);
    void push_back(Key key, Info info);
    bool pop_front();
    bool pop_back();
    bool deleteNode(const Key& key, const Info& info); //deletes all nodes with given key and info
    bool deleteNode(Node<Key, Info>* node); //deletes a node by its pointer
    void clearSequence(); //delete all items from sequence
};

template <typename Key, typename Info>
Sequence<Key, Info>::Sequence()
{
    head = nullptr;
    sequenceSize = 0;
}

template <typename Key, typename Info>
Sequence<Key, Info>::Sequence(const Sequence& src)
{
    head = nullptr;
    sequenceSize = src.sequenceSize;
    Node<Key, Info>* currentsrc = src.head;
    while (currentsrc != nullptr)
    {
        push_back(currentsrc->key, currentsrc->info);
        currentsrc = currentsrc->next;
    }
}

template <typename Key, typename Info>
void Sequence<Key, Info>::clearSequence()
{
    while (head != nullptr)
    {
        Node<Key, Info>* temp = head;
        head = head->next;
        delete temp;
    }
    sequenceSize = 0;
}

template <typename Key, typename Info>
Sequence<Key, Info>& Sequence<Key, Info>::operator=(const Sequence& src)
{
    if (this == &src)
        return *this;
    sequenceSize = src.sequenceSize;

    clearSequence();

    Node<Key, Info>* currentsrc = src.head;
    while (currentsrc != nullptr)
    {
        push_back(currentsrc->key, currentsrc->info);
        currentsrc = currentsrc->next;
    }

    return *this;
}

template <typename Key, typename Info>
Sequence<Key, Info>& Sequence<Key, Info>::operator+(const Sequence& src)
{
    Node<Key, Info>* current = this->getTail();
    Node<Key, Info>* currentsrc = src.head;
    while (currentsrc != nullptr)
    {
        push_back(currentsrc->key, currentsrc->info);
        currentsrc = currentsrc->next;
    }

    return *this;
}

template <typename Key, typename Info>
Sequence<Key, Info>& Sequence<Key, Info>::operator+=(const Sequence& src)
{
    Node<Key, Info>* current = this->getTail();
    Node<Key, Info>* currentsrc = src.head;
    while (currentsrc != nullptr)
    {
        push_back(currentsrc->key, currentsrc->info);
        currentsrc = currentsrc->next;
    }

    return *this;
}

template <typename Key, typename Info>
unsigned int Sequence<Key, Info>::getSequenceSize() const
{
    return sequenceSize;
}

template <typename Key, typename Info>
Node<Key, Info>* Sequence<Key, Info>::getHead()
{
    return head;
}

template <typename Key, typename Info>
Node<Key, Info>* Sequence<Key, Info>::getTail()
{
    if (isEmpty())
    {
        return nullptr;
    }
    Node<Key, Info>* current = head;
    while (current->next != nullptr)
    {
        current = current->next;
    }
    
    return current;
}

template <typename Key, typename Info>
void Sequence<Key, Info>::display()
{
    if(!isEmpty())
    {
        std::cout << "Sequence: " << this << std::endl;
        Node<Key, Info>* current = head;
        while(current != nullptr)
        {
            std::cout << "Key: " << current->key << "\tInfo: " << current->info << std::endl;
            current = current->next;
        }
    }
}

template <typename Key, typename Info>
bool Sequence<Key, Info>::isEmpty()
{
    if(!head)
        return true;
    else
        return false;
}

template <typename Key, typename Info>
Node<Key, Info>* Sequence<Key, Info>::find(const Key& key, const Info& info)
{
    if(isEmpty())
        return nullptr;
    else
    {
        Node<Key, Info>* current = head;
        while (current != nullptr)
        {
            if(current->key == key && current->info == info)
                return current;
            current = current->next;
        }
        return nullptr;
    }
}

template <typename Key, typename Info>
void Sequence<Key, Info>::push_front(Key key, Info info)
{
    Node<Key, Info>* newNode = new Node<Key, Info>(key, info);
    newNode->next = head;
    head = newNode;
    sequenceSize++;
}

template <typename Key, typename Info>
void Sequence<Key, Info>::push_back(Key key, Info info)
{
    Node<Key, Info>* newNode = new Node<Key, Info>(key, info);
    if (isEmpty())
    {
        head = newNode;
    }
    else
    {
        Node<Key, Info>* tail = getTail();
        tail->next = newNode;
    }
    sequenceSize++;
}

template <typename Key, typename Info>
bool Sequence<Key, Info>::pop_front()
{
    if(isEmpty())
        return false;
    else
    {
        Node<Key, Info>* newHead = head->next;
        delete head;
        head = newHead;
        sequenceSize--;
        return true;
    }
}

template <typename Key, typename Info>
bool Sequence<Key, Info>::pop_back()
{
    if(isEmpty())
        return false;
    else if(head->next == nullptr)
    {
        delete head;
        head = nullptr;
        sequenceSize--;
        return true;
    }
    else
    {
        Node<Key, Info>* current = head;
        while(current->next->next != nullptr)
            current = current->next; 
        delete current->next;
        current->next = nullptr;
        sequenceSize--;
        return true;
    }
}

template <typename Key, typename Info>
bool Sequence<Key, Info>::deleteNode(const Key& key, const Info& info)
{
    bool result = false;
    if(isEmpty())
        return false;
    Node<Key, Info>* current = head;
    while (current != nullptr && current->next != nullptr)
    {
        if(current->next->key == key && current->next->info == info)
        {
            Node<Key, Info>* temp = current->next;
            current->next = current->next->next;
            delete temp;
            sequenceSize--;
            result = true;
        }
        else
        {
            current = current->next;
        }
    }
    if(head->key == key && head->info == info)
    {
        Node<Key, Info>* temp = head;
        head = head->next;
        delete temp;
        sequenceSize--;
        result = true;
    }
    return result;
}

template <typename Key, typename Info>
bool Sequence<Key, Info>::deleteNode(Node<Key, Info>* node)
{
    if(isEmpty())
        return false;
    if(head == node)
    {
        Node<Key, Info>* temp = head;
        head = head->next;
        delete temp;
        sequenceSize--;
        return true;
    }
    Node<Key, Info>* current = head;
    while (current != nullptr && current->next != nullptr)
    {
        if(current->next == node)
        {
            Node<Key, Info>* temp = current->next;
            current->next = current->next->next;
            delete temp;
            sequenceSize--;
            return true;
        }
        current = current->next;
    }
    return false;
}

template <typename Key, typename Info>
Sequence<Key, Info>::~Sequence()
{
    if(!isEmpty())
    {
        clearSequence();
    }    
}
