//argv = 't' - runs tests
//argv = 'v' - runs tests and outputs function results

#include "Sequence.h"
#include <iostream>
#include <cassert>

//Function to fill sequence using array elements as keys
template <typename Key, typename Info>
void fillSeqWithArray(Sequence <Key, Info>& seq, const Key arr[], int size, Info info)
{
    for (int i = 0; i < size; i++) {
        seq.push_back(arr[i], info);
    }
}

//Function 1
template <typename Key, typename Info>
void split_pos(Sequence<Key, Info>& seq, int start_pos, int len1, int len2, int count, Sequence<Key, Info>& seq1, Sequence<Key, Info>& seq2)
{
    if(seq.isEmpty())
        return;
    Node<Key, Info>* sp = seq.getHead();
    for(int i = 0; i != start_pos; i++)
    {
        if(!sp->next)
            return;
        sp = sp->next;
    }
    Node<Key, Info>* current = sp;

    for(int i = 0; i < count; i++)
    {
        if(current == nullptr)
            return;
        for(int j = 0; j < len1; j++)
        {
            if(current == nullptr)
                return;
            seq1.push_back(current->key, current->info);
            Node<Key, Info>* currentcp = current;
            current = current->next;
            seq.deleteNode(currentcp);
        }
        for(int j = 0; j < len2; j++)
        {
            if(current == nullptr)
                return;
            seq2.push_back(current->key, current->info);
            Node<Key, Info>* currentcp = current;
            current = current->next;
            seq.deleteNode(currentcp);
        }
    }
}

//Function 2
template <typename Key, typename Info>
void split_key( Sequence <Key, Info>& seq, const Key& start_key, int start_occ, int len1, int len2, int count, Sequence <Key, Info>& seq1, Sequence <Key, Info>& seq2)
{
    if(seq.isEmpty())
        return;
    Node<Key, Info>* sp = seq.getHead();
    int counter = 0;
    while (sp != nullptr)
    {
        if (sp->key == start_key) 
        {
            counter++;
            if (counter == start_occ)
            {
                break;
            }
        }
    if(!sp->next)
        return;
    sp = sp->next;
    }
    Node<Key, Info>* current = sp;
    for(int i = 0; i < count; i++)
    {
        if(current == nullptr)
            return;
        for(int j = 0; j < len1; j++)
        {
            if(current == nullptr)
                return;
            seq1.push_back(current->key, current->info);
            Node<Key, Info>* currentcp = current;
            current = current->next;
            seq.deleteNode(currentcp);
        }
        for(int j = 0; j < len2; j++)
        {
            if(current == nullptr)
                return;
            seq2.push_back(current->key, current->info);
            Node<Key, Info>* currentcp = current;
            current = current->next;
            seq.deleteNode(currentcp);
        }
    }
}

//METHOD TESTS

void test_push_front()
{
    Sequence<int, char> seq;
    seq.push_front(1, 'a');
    seq.push_front(2, 'b');
    assert(seq.getSequenceSize() == 2);
    Node<int, char>* newHead = seq.getHead();
    assert(newHead->key == 2);
    assert(newHead->info == 'b');
}

void test_push_back()
{
    Sequence<int, char> seq;
    seq.push_back(1, 'a');
    seq.push_back(2, 'b');
    seq.push_back(3, 'c');
    assert(seq.getSequenceSize() == 3);
    Node<int, char>* tail = seq.getTail();
    assert(tail->key == 3);
    assert(tail->info == 'c');
}

void test_pop_front()
{
    Sequence<int, char> seq;
    seq.push_back(1, 'a');
    seq.push_back(2, 'b');
    seq.push_back(3, 'c');
    seq.pop_front();
    assert(seq.getSequenceSize() == 2);
    Node<int, char>* newHead = seq.getHead();
    assert(newHead->key == 2);
    assert(newHead->info == 'b');
}

void test_pop_back()
{
    Sequence<int, char> seq;
    seq.push_back(1, 'a');
    seq.push_back(2, 'b');
    seq.push_back(3, 'c');
    assert(seq.getSequenceSize() == 3);
    seq.pop_back();
    assert(seq.getSequenceSize() == 2);
    Node<int, char>* newTail = seq.getTail();
    assert(newTail->key == 2);
    assert(newTail->info == 'b');
}

void test_find()
{
    Sequence<int, char> seq;
    seq.push_back(1, 'a');
    seq.push_back(2, 'b');
    seq.push_back(3, 'c');
    Node<int, char>* found = seq.find(2, 'b');
    assert(seq.find(2, 'b') == found);
    assert(seq.find(4, 'd') == nullptr);
}

void test_isEmpty()
{
    Sequence<int, char> seq;
    assert(seq.isEmpty() == true);
    seq.push_back(1, 'a');
    assert(seq.isEmpty() == false);
}

void test_clearSequence()
{
    Sequence<int, char> seq;
    seq.push_back(1, 'a');
    seq.push_back(2, 'b');
    seq.push_back(3, 'c');
    assert(seq.getSequenceSize() == 3);
    seq.clearSequence();
    assert(seq.getSequenceSize() == 0);
    assert(seq.isEmpty() == true);
}

void test_deleteNode()
{
    Sequence<int, char> seq;
    seq.push_back(1, 'a');
    seq.push_back(2, 'b');
    seq.push_back(2, 'b');
    seq.push_back(2, 'b');
    seq.push_back(3, 'c');
    assert(seq.getSequenceSize() == 5);
    seq.deleteNode(2, 'b');
    assert(seq.getSequenceSize() == 2);
    assert(seq.find(2, 'b') == nullptr);
}

void test_deleteNodeByPointer()
{
    Sequence<int, char> seq;
    seq.push_back(1, 'a');
    seq.push_back(2, 'b');
    seq.push_back(3, 'c');
    seq.deleteNode(seq.find(2, 'b'));
    assert(seq.getSequenceSize() == 2);
    assert(seq.find(2, 'b') == nullptr);
}

void test_assign_operator()
{
    Sequence<int, char> seq;
    seq.push_back(1, 'a');
    seq.push_back(2, 'b');
    seq.push_back(3, 'c');
    seq.push_back(4, 'd');
    seq.push_back(5, 'e');
    Sequence<int, char> seq2;
    seq2 = seq;
    assert(seq.getSequenceSize() == seq2.getSequenceSize());
    Node<int, char>* current = seq.getHead();
    Node<int, char>* current2 = seq2.getHead();
    while (current != nullptr)
    {
        assert(current->key == current2->key);
        assert(current->info == current2->info);
        current = current->next;
        current2 = current2->next;
    }
}

void test_sum_operator()
{
    Sequence<int, char> seq;
    seq.push_back(1, 'a');
    seq.push_back(2, 'b');
    seq.push_back(3, 'c');
    Sequence<int, char> seq2;
    seq2.push_back(4, 'd');
    seq2.push_back(5, 'e');
    seq = seq + seq2;
    assert(seq.getSequenceSize() == 5);
    Node<int, char>* current = seq.getHead();
    for(int i = 1; i < seq.getSequenceSize(); i++)
    {
        assert(current->key == i);
        current = current->next;
    }
}

void test_assign_sum_operator()
{
    Sequence<int, char> seq;
    seq.push_back(1, 'a');
    seq.push_back(2, 'b');
    seq.push_back(3, 'c');
    Sequence<int, char> seq2;
    seq2.push_back(4, 'd');
    seq2.push_back(5, 'e');
    seq += seq2;
    assert(seq.getSequenceSize() == 5);
    Node<int, char>* current = seq.getHead();
    for(int i = 1; i < seq.getSequenceSize(); i++)
    {
        assert(current->key == i);
        current = current->next;
    }
}

void test_methods()
{
    test_push_front();
    test_push_back();
    test_pop_front();
    test_pop_back();
    test_find();
    test_isEmpty();
    test_clearSequence();
    test_deleteNode();
    test_deleteNodeByPointer();
    test_assign_operator();
    test_sum_operator();
    test_assign_sum_operator();
    std::cout << "ALL METHODS TESTS PASSED\n" << std::endl;
}

//FUNCTIONS TESTS

void test_fillSeqWithArray()
{
    int arr[5] = {5, 5, 5, 5, 5};
    Sequence<int, char> a;
    fillSeqWithArray(a, arr, 5, 'f');
    a.deleteNode(5, 'f');
    assert(a.getSequenceSize() == 0);
}

void test_split_pos(bool output)
{
    Sequence<int, char> a;
    Sequence<int, char> b;
    Sequence<int, char> c;
    for(int i = 0; i < 11; i++)
    {
        a.push_back(i, 'a');
    }
    if(output)
    {
        std::cout << "INITIAL SEQUENCE:\n";
        a.display();
    }
    split_pos(a, 2, 2, 3, 4, b, c);

    assert(a.getSequenceSize() == 2);
    assert(b.getSequenceSize() == 4);
    assert(c.getSequenceSize() == 5);

    int seq[2] = {0,1};
    int seq1[4] = {2,3,7,8};
    int seq2[5] = {4,5,6,9,10};

    Node<int, char>* current = a.getHead();
    for(int i = 0; i < a.getSequenceSize(); i++)
    {
        assert(seq[i] == current->key);
        current = current->next;
    }
    current = b.getHead();
    for(int i = 0; i < b.getSequenceSize(); i++)
    {
        assert(seq1[i] == current->key);
        current = current->next;
    }
    current = c.getHead();
    for(int i = 0; i < c.getSequenceSize(); i++)
    {
        assert(seq2[i] == current->key);
        current = current->next;
    }
    if(output)
    {
        std::cout << "\nRESULT SEQUENCES:\n";
        a.display();
        b.display();
        c.display();
        std::cout << std::endl;
    }
}

void test_split_key(bool output)
{
    int arr[25] = {0,1,2,3,4,5,6,4,8,9,4,11,12,2,14,15,11,17,23,19,20,21,22,23,24};
    int seq[15] = {0,1,2,3,4,5,6,17,23,19,20,21,22,23,24};
    int seq1[6] = {4,8,9,12,2,14};
    int seq2[4] = {4,11,15,11};

    Sequence<int, char> a;
    Sequence<int, char> b;
    Sequence<int, char> c;
    fillSeqWithArray(a, arr, 25, 'a');

    if(output)
    {
        std::cout << "INITIAL SEQUENCE:\n";
        a.display();
    }
    
    split_key(a, 4, 2, 3, 2, 2, b, c);
    
    assert(a.getSequenceSize() == 15);
    assert(b.getSequenceSize() == 6);
    assert(c.getSequenceSize() == 4);

    Node<int, char>* current = a.getHead();
    for(int i = 0; i < a.getSequenceSize(); i++)
    {
        assert(seq[i] == current->key);
        current = current->next;
    }
    current = b.getHead();
    for(int i = 0; i < b.getSequenceSize(); i++)
    {
        assert(seq1[i] == current->key);
        current = current->next;
    }
    current = c.getHead();
    for(int i = 0; i < c.getSequenceSize(); i++)
    {
        assert(seq2[i] == current->key);
        current = current->next;
    }
    if(output)
    {
        std::cout << "\nRESULT SEQUENCES:\n";
        a.display();
        b.display();
        c.display();
        std::cout << std::endl;
    }
}

void test_functions(bool output) // true to output results
{
    test_fillSeqWithArray();
    test_split_pos(output);
    test_split_key(output);
    std::cout << "ALL FUNCTIONS TESTS PASSED\n" <<std::endl;
}

int main(int argc, char *argv[])
{
    bool output_func_tests;
    if(argc >= 2)
    {
        if(argv[1][0] == 'v')
            output_func_tests = true;
        if(argv[1][0] == 't')
            output_func_tests = false;
        test_methods();
        test_functions(output_func_tests); 
    }
    return 0;
}