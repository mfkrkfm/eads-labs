# Algorithm and Data Structure Task #3

There are several steps to complete:
* Design avl_tree class according to very limited specification below.
* Implementing your class write unit tests in parallel (try to write test before actual implementation of the behaviour of your tree).
* You should implement your class template in the avl_tree.h file, and provide tests of your container in avl_tree_test.h and avl_tree_test.cpp files.
* Implement function template maxinfo_selector.
* Implement function count_words.
* Be prepared to modify fragments of your solution or write additional function/template during the lab class on January 10th.

## PART1 - CLASS DESIGN

Design a class to represent AVL tree. Write unit tests for the designed class, at least one test per method.

`````c++
template <typename Key, typename Info>
class avl_tree
{
public:
	avl_tree();
	avl_tree (const avl_tree& src);
	~avl_tree();
	avl_tree& operator=(const avl_tree& src);

	// insert (key, info) pair

	// remove given key

	// check if given key is present

	// print nicely formatted tree structure

	Info& operator[](const Key& key);
	const Info& operator[](const Key& key) const;

	// what else can be useful in such a collection?
};
`````

## PART 2 – ADDITIONAL FUNCTIONS

Implement two additional functions:
* *maxinfo_selector*,
* *count_words*.

### Function *maxinfo_selector*

`````c++
template <typename Key, typename Info>
std::vector<std::pair<Key, Info>> 
	maxinfo_selector(const avl_tree<Key, Info>& tree, unsigned cnt);
`````


Function *maxinfo_selector* returns *cnt* elements of the tree with the highest values of the info member.

### Function *count_words*

`````c++
avl_tree<string, int> count_words(istream& is);
`````

Function *count_words* creates a dictionary mapping words found in the is stream to number of occurrences of the word in this stream. You can use beagle_voyage.txt (yes, this is The Voyage of the Beagle by Charles Darwin as published on the Gutenberg Project page - https://www.gutenberg.org/) as a slightly larger test for your count_words function.

How the execution time of your count_words compares to the word counter implemented with the map collection from the standard library?
You can use following code to measure the execution time (don’t forget to include chrono header):

`````c++
for (int rep = 0; rep < 5; ++rep)
{
    ifstream is("beagle_voyage.txt");
    if (!is)
    {
        cout << "Error opening input file.\n";
        return 1;
    }

    auto start_time = std::chrono::high_resolution_clock::now();

    string word;
    map<string, int> wc;  // counting word occurrences in the stream
    while (is >> word)
        wc[word]++;

    auto end_time = std::chrono::high_resolution_clock::now();
    auto time = end_time - start_time;

    std::cout << "Ellapsed time: " << time/std::chrono::milliseconds(1) << " ms.\n";
}
`````

If the computations are not lengthy it is good to repeat time measurements several times. The result on my machine is following:
Ellapsed time: 40 ms.
Ellapsed time: 41 ms.
Ellapsed time: 49 ms.
Ellapsed time: 42 ms.
Ellapsed time: 43 ms.

(My avl_tree implementation is slightly slower).
