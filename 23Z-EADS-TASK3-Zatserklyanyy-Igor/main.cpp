#include <chrono>
#include "AVLTree.h"

int main()
{
    std::ifstream is("beagle_voyage.txt");
    count_words(is).printTree();
    for (int rep = 0; rep < 5; ++rep) 
    {
        std::ifstream is("beagle_voyage.txt");
        if (!is) {
            std::cout << "Error opening input file.\n";
            return 1;
        }

        auto start_time = std::chrono::high_resolution_clock::now();

        std::map<std::string, int> wc;
        std::string word;
        while (is >> word)
            wc[word]++;

        auto end_time = std::chrono::high_resolution_clock::now();
        auto time = end_time - start_time;
        std::cout << "Map ellapsed time: " << time/std::chrono::milliseconds(1) << " ms.\n";

        is.close();

        std::ifstream is2("beagle_voyage.txt");
        if (!is2) {
            std::cout << "Error opening input file.\n";
            return 1;
        }

        start_time = std::chrono::high_resolution_clock::now();

        auto wordCountAVL = count_words(is2);

        end_time = std::chrono::high_resolution_clock::now();
        time = end_time - start_time;
        std::cout << "AVL tree ellapsed time: " << time/std::chrono::milliseconds(1) << " ms.\n";

        is2.close();
    }   
    TestAVLTree<int, std::string> test;
    std::cout << "all tests passed\n";
    return 0;
}
