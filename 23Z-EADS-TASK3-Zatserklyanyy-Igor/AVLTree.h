#ifndef AVL_TREE_H
#define AVL_TREE_H
#include <iostream>
#include <vector>
#include <algorithm>
#include <fstream>
#include <string>
#include <cassert>
#include <map>

template <typename Key, typename Info>
class TestAVLTree;

template <typename Key, typename Info>
class AVLTree
{
private:
    struct Node
    {
        Key key;
        Info info;
        Node* left;
        Node* right;
        int height;
        Node(const Key& k, const Info& i);
    };
    Node* root;
    Node* getRoot() const;
    Node* rotateRight(Node* y);
    Node* rotateLeft(Node* x);
    int getHeight(Node* n);
    int getBalance(Node* n);
    Node* insertNode(Node* node, const Key& key, const Info& info);
    Node* minValueNode(Node* node);
    Node* deleteNode(Node* root, const Key& key);
    void inOrderPrint(Node* root) const;
    void deleteTree(Node* node);
    Node* copyTree(Node* node) const;
    void inOrderCollect(std::vector<std::pair<Key, Info>>& nodes, Node* node) const;
    friend class TestAVLTree<Key, Info>; // Add this line
public:
    AVLTree();
    AVLTree(const AVLTree& src);
    ~AVLTree();
    AVLTree& operator=(const AVLTree& src);
    AVLTree operator-(const AVLTree& src);
    bool insert(const Key& key, const Info& info);
    bool remove(const Key& key);
    bool contains(const Key& key) const;
    void printTree() const;
    Info& operator[](const Key& key);
    Info& getSetInfo(const Key& key);
    void inOrderCollect(std::vector<std::pair<Key, Info>>& nodes) const;
};

template <typename Key, typename Info>
AVLTree<Key, Info>::Node::Node(const Key& k, const Info& i)
{
    key = k;
    info = i;
    left = nullptr;
    right = nullptr;
    height = 1;
}

template <typename Key, typename Info>
AVLTree<Key, Info>::Node* AVLTree<Key, Info>::rotateRight(Node* y)
{
    Node* x = y->left;
    Node* T2 = x->right;
    x->right = y;
    y->left = T2;
    y->height = std::max(getHeight(y->left), getHeight(y->right)) + 1;
    x->height = std::max(getHeight(x->left), getHeight(x->right)) + 1;
    return x;
}

template <typename Key, typename Info>
AVLTree<Key, Info>::Node* AVLTree<Key, Info>::rotateLeft(Node* x)
{
    Node* y = x->right;
    Node* T2 = y->left;
    y->left = x;
    x->right = T2;
    x->height = std::max(getHeight(x->left), getHeight(x->right)) + 1;
    y->height = std::max(getHeight(y->left), getHeight(y->right)) + 1;
    return y;
}

template <typename Key, typename Info>
int AVLTree<Key, Info>::getHeight(Node* n)
{
    if (n == nullptr)
        return 0;
    return n->height;
}

template <typename Key, typename Info>
int AVLTree<Key, Info>::getBalance(Node* n)
{
    if (n == nullptr)
        return 0;
    return getHeight(n->left) - getHeight(n->right);
}

template <typename Key, typename Info>
AVLTree<Key, Info>::Node* AVLTree<Key, Info>::insertNode(Node* node, const Key& key, const Info& info)
{
    if (node == nullptr)
        return new Node(key, info);
    if (key < node->key)
        node->left = insertNode(node->left, key, info);
    else if (key > node->key)
        node->right = insertNode(node->right, key, info);
    else
        return node;
    node->height = 1 + std::max(getHeight(node->left), getHeight(node->right));
    int balance = getBalance(node);
    if (balance > 1 && key < node->left->key)
        return rotateRight(node);
    if (balance < -1 && key > node->right->key)
        return rotateLeft(node);
    if (balance > 1 && key > node->left->key)
    {
        node->left = rotateLeft(node->left);
        return rotateRight(node);
    }
    if (balance < -1 && key < node->right->key)
    {
        node->right = rotateRight(node->right);
        return rotateLeft(node);
    }
    return node;
}

template <typename Key, typename Info>
AVLTree<Key, Info>::Node* AVLTree<Key, Info>::minValueNode(Node* node)
{
    Node* current = node;
    while (current->left != nullptr)
        current = current->left;
    return current;
}

template <typename Key, typename Info>
AVLTree<Key, Info>::Node* AVLTree<Key, Info>::deleteNode(Node* root, const Key& key)
{
    if (root == nullptr)
        return root;
    if ( key < root->key )
        root->left = deleteNode(root->left, key);
    else if( key > root->key )
        root->right = deleteNode(root->right, key);
    else
    {
        if((root->left == nullptr) || (root->right == nullptr)) 
        {
            Node *temp = root->left ? root->left : root->right;
            if (temp == nullptr) 
            {
                temp = root;
                root = nullptr;
            }
            else
            *root = *temp;
            delete temp;
        }
        else
        {
            Node* temp = minValueNode(root->right);
            root->key = temp->key;
            root->info = temp->info;
            root->right = deleteNode(root->right, temp->key);
        }
    }
    if (root == nullptr)
        return root;
    root->height = 1 + std::max(getHeight(root->left), getHeight(root->right));
    int balance = getBalance(root);
    if (balance > 1 && getBalance(root->left) >= 0)
        return rotateRight(root);
    if (balance > 1 && getBalance(root->left) < 0)
    {
        root->left = rotateLeft(root->left);
        return rotateRight(root);
    }
    if (balance < -1 && getBalance(root->right) <= 0)
        return rotateLeft(root);
    if (balance < -1 && getBalance(root->right) > 0)
    {
        root->right = rotateRight(root->right);
        return rotateLeft(root);
    }
    return root;
}

template <typename Key, typename Info>
void AVLTree<Key, Info>::inOrderPrint(Node* root) const
{
    if (root != nullptr)
    {
        inOrderPrint(root->left);
        std::cout << root->key << " " << root->info << "\n";
        inOrderPrint(root->right);
    }
}

template <typename Key, typename Info>
void AVLTree<Key, Info>::deleteTree(Node* node)
{
    if (node == nullptr) return;
    deleteTree(node->left);
    deleteTree(node->right);
    delete node;
}

template <typename Key, typename Info>
AVLTree<Key, Info>::Node* AVLTree<Key, Info>::copyTree(Node* node) const
{
    if (node == nullptr)
        return nullptr;
    Node* newNode = new Node(node->key, node->info);
    newNode->left = copyTree(node->left);
    newNode->right = copyTree(node->right);
    newNode->height = node->height;
    return newNode;
}

template <typename Key, typename Info>
AVLTree<Key, Info>::AVLTree()
{
    root = nullptr;
}

template <typename Key, typename Info>
AVLTree<Key, Info>::AVLTree(const AVLTree& src)
{
    root = nullptr;
    root = copyTree(src.root);
}

template <typename Key, typename Info>
AVLTree<Key, Info>::~AVLTree()
{
    deleteTree(root);
}

template <typename Key, typename Info>
AVLTree<Key, Info>& AVLTree<Key, Info>::operator=(const AVLTree& src)
{
    if (this == &src) return *this;

    deleteTree(root);
    root = copyTree(src.root);

    return *this;
}

template <typename Key, typename Info>
AVLTree<Key, Info> AVLTree<Key, Info>::operator-(const AVLTree& src)
{
    AVLTree<Key, Info> newTree;
    if (this == &src)
        return newTree;
    std::vector<std::pair<Key, Info>> nodes;
    inOrderCollect(nodes);
    for(const auto& node : nodes)
    {
        if(!src.contains(node.first))
            newTree.insert(node.first, node.second);
    }
    return newTree;
}


template <typename Key, typename Info>
bool AVLTree<Key, Info>::insert(const Key& key, const Info& info)
{
    if (contains(key))
    {
        return false;
    }

    root = insertNode(root, key, info);
    return true;
}

template <typename Key, typename Info>
bool AVLTree<Key, Info>::remove(const Key& key)
{
    if (!contains(key))
    {
        return false;
    }
    root = deleteNode(root, key);
    return true;
}

template <typename Key, typename Info>
bool AVLTree<Key, Info>::contains(const Key& key) const
{
    Node* current = root;
    while (current != nullptr)
    {
        if (key < current->key)
            current = current->left;
        else if (key > current->key)
            current = current->right;
        else
            return true;
    }
    return false;
}

template <typename Key, typename Info>
void AVLTree<Key, Info>::printTree() const
{
    inOrderPrint(root);
    std::cout << "\n";
}

template <typename Key, typename Info>
Info& AVLTree<Key, Info>::operator[](const Key& key)
{
    Node* current = root;
    while (current != nullptr)
    {
        if (key < current->key)
            current = current->left;
        else if (key > current->key)
            current = current->right;
        else
            return current->info;
    }
    return root->info;
}

template <typename Key, typename Info>
Info& AVLTree<Key, Info>::getSetInfo(const Key& key)
{
    Node* current = root;
    while (current != nullptr)
    {
        if (key < current->key)
            current = current->left;
        else if (key > current->key)
            current = current->right;
        else
            return current->info;
    }
    current = insertNode(root, key, 1);
    return current->info;
}

template <typename Key, typename Info>
AVLTree<Key, Info>::Node* AVLTree<Key, Info>::getRoot() const
{
    return root;
}

template <typename Key, typename Info>
void AVLTree<Key, Info>::inOrderCollect(std::vector<std::pair<Key, Info>>& nodes) const 
{
    inOrderCollect(nodes, root);
}

template <typename Key, typename Info>
void AVLTree<Key, Info>::inOrderCollect(std::vector<std::pair<Key, Info>>& nodes, Node* node) const
{
    if (node != nullptr) {
        inOrderCollect(nodes, node->left);
        nodes.emplace_back(node->key, node->info);
        inOrderCollect(nodes, node->right);
    }
}

//EXTERNAL FUNCTIONS

template <typename Key, typename Info>
std::vector<std::pair<Key, Info>> maxinfo_selector(const AVLTree<Key, Info>& tree, unsigned cnt)
{
    std::vector<std::pair<Key, Info>> nodes;
    tree.inOrderCollect(nodes);

    std::sort(nodes.begin(), nodes.end(), [](const std::pair<Key, Info>& a, const std::pair<Key, Info>& b)
    {
        return a.second > b.second;
    });
    if (nodes.size() > cnt)
    {
        nodes.resize(cnt);
    }

    return nodes;
}

template <typename Key>
std::vector<std::pair<Key, std::string>> maxinfo_selector(const AVLTree<Key, std::string>& tree, unsigned cnt)
{
    std::vector<std::pair<Key, std::string>> nodes;
    tree.inOrderCollect(nodes);

    std::sort(nodes.begin(), nodes.end(), [](const std::pair<Key, std::string>& a, const std::pair<Key, std::string>& b)
    {
        return a.second.length() > b.second.length();
    });
    if (nodes.size() > cnt)
    {
        nodes.resize(cnt);
    }

    return nodes;
}

AVLTree<std::string, int> count_words(std::istream& is) {
    AVLTree<std::string, int> wordCount;
    std::string word;
    while (is >> word) {
        if (wordCount.contains(word)) {
            int& count = wordCount.getSetInfo(word);
            count++;
        } else {
            wordCount.insert(word, 1);
        }
    }
    return wordCount;
}

//TESTS
template <typename Key, typename Info>
class TestAVLTree {
private:
    AVLTree<Key, Info> tree;

    void testConstructor() {
        assert(tree.getRoot() == nullptr);
    }

    void testInsert()
    {
        assert(tree.insert(10, "Ten"));
        assert(tree.getRoot() != nullptr);
        assert(tree.getRoot()->key == 10);
        assert(tree.insert(20, "Twenty"));
        assert(tree.insert(5, "Five"));
        assert(tree.getRoot()->left->key == 5);
        assert(tree.getRoot()->right->key == 20);
    }

    void testRemove()
    {
        assert(tree.remove(5));
        assert(tree.getRoot()->left == nullptr);
    }

    void testCopyConstructor()
    {
        AVLTree<Key, Info> treeCopy(tree);
        assert(treeCopy.getRoot()->key == 10);
        assert(treeCopy.getRoot()->right->key == 20);
    }

    void testAssignmentOperator()
    {
        tree.insert(5, "Five");
        AVLTree<Key, Info> treeCopy;
        treeCopy = tree;
        assert(treeCopy.getRoot()->key == 10);
        assert(treeCopy.getRoot()->left->key == 5);
        assert(treeCopy.getRoot()->right->key == 20);
    }
    void testSubstractionOperator()
    {
        AVLTree<Key, Info> newTree;
        assert(newTree.insert(10, "Ten"));
        assert(newTree.insert(5, "Five"));
        assert((tree - newTree).getRoot()->key == 20);
        assert((tree - newTree).getRoot()->left == nullptr);
        assert((tree - newTree).getRoot()->right == nullptr);

    }
    void testMaxInfoSelector()
    {
        tree.insert(89, "Eighty-Nine");
        std::vector<std::pair<int, std::string>> maxInfoElements = maxinfo_selector(tree, 3);
        assert(maxInfoElements[0].second == "Eighty-Nine");
        assert(maxInfoElements[1].second == "Twenty");
        assert(maxInfoElements[2].second == "Five");
        AVLTree<int, int> tree2;
        tree2.insert(1, 1);
        tree2.insert(2, 2);
        tree2.insert(3, 3);
        tree2.insert(4, 4);
        std::vector<std::pair<int, int>> maxInfoElements2 = maxinfo_selector(tree2, 3);
        assert(maxInfoElements2[0].second == 4);
        assert(maxInfoElements2[1].second == 3);
        assert(maxInfoElements2[2].second == 2);
    }

public:
    TestAVLTree()
    {
        testConstructor();
        testInsert();
        testRemove();
        testCopyConstructor();
        testAssignmentOperator();
        testSubstractionOperator();
        testMaxInfoSelector();
    }
};



#endif